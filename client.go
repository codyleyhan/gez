package gez

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"strings"

	jsoniter "github.com/json-iterator/go"
	spb "google.golang.org/genproto/googleapis/rpc/status"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var _ grpc.ClientConnInterface = (*Client)(nil)

type Client struct {
	endpoint string
	client   *http.Client
}

func NewClient(client *http.Client, endpoint string) *Client {
	return &Client{
		endpoint: endpoint,
		client:   client,
	}
}

type httpResponse struct {
	Status *spb.Status     `json:"status,omitempty"`
	Data   json.RawMessage `json:"data,omitempty"`
}

type httpRequest struct {
	Method string      `json:"method,omitempty"`
	Data   interface{} `json:"data,omitempty"`
}

func (c *Client) Invoke(ctx context.Context, method string, args interface{}, reply interface{}, opts ...grpc.CallOption) error {
	b := bytes.NewBuffer(nil)
	request := httpRequest{
		Method: method,
		Data:   args,
	}
	if err := jsoniter.NewEncoder(b).Encode(request); err != nil {
		return status.Newf(codes.Internal, "failed to convert request to JSON: %w", err).Err()
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, c.endpoint, b)
	if err != nil {
		return status.Newf(codes.Internal, "failed to create request for RPC: %w", err).Err()
	}
	md, ok := metadata.FromOutgoingContext(ctx)
	if ok {
		for k := range md {
			req.Header.Add(k, strings.Join(md.Get(k), ","))
		}
	}

	res, err := c.client.Do(req)
	if err != nil {
		return status.Newf(codes.Unknown, "failed to make RPC request: %w", err).Err()
	}
	defer res.Body.Close()

	for _, opt := range opts {
		switch t := opt.(type) {
		case grpc.HeaderCallOption:
			headers := make(map[string]string)
			for k, v := range res.Header {
				headers[k] = strings.Join(v, ",")
			}
			*t.HeaderAddr = metadata.New(headers).Copy()
		}
	}

	if res.StatusCode != http.StatusOK {
		return status.Newf(codes.Unknown, "RPC request returned unexpected HTTP status code: %d", res.StatusCode).Err()
	}
	if res.Header.Get("Content-Type") != "application/json" {
		return status.New(codes.Unknown, "RPC request did not return valid JSON").Err()
	}

	var response httpResponse
	if err := json.NewDecoder(res.Body).Decode(&response); err != nil {
		return status.Newf(codes.Unknown, "unable to decode response body JSON: %w", err).Err()
	}
	s := status.FromProto(response.Status)
	if s.Code() != codes.OK {
		return s.Err()
	}

	if response.Data == nil {
		return status.New(codes.Unknown, "response did not include RPC response data").Err()
	}

	if jsoniter.Unmarshal(response.Data, reply); err != nil {
		return status.Newf(codes.Unknown, "failed to unmarshal response data into reply: %w", err).Err()
	}

	return nil
}

func (c *Client) NewStream(ctx context.Context, desc *grpc.StreamDesc, method string, opts ...grpc.CallOption) (grpc.ClientStream, error) {
	return nil, status.New(codes.Internal, "streams are not supported in gez").Err()

}
