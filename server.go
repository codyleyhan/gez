package gez

import (
	"context"
	"log"
	"net/http"
	"reflect"
	"strings"

	jsoniter "github.com/json-iterator/go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
)

var _ grpc.ServiceRegistrar = (*Server)(nil)

type methodHandler func(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error)

type method struct {
	name    string
	handler methodHandler
}

type service struct {
	name    string
	svc     interface{}
	methods map[string]*method
}

type Server struct {
	services    map[string]*service
	interceptor grpc.UnaryServerInterceptor
	opts        opts
}

type optFn func(*opts)

func WithInterceptor(i grpc.UnaryServerInterceptor) optFn {
	return func(o *opts) {
		o.interceptor = i
	}
}

func WithIgnoreServicesWithStreamError() optFn {
	return func(o *opts) {
		o.ignoreStreamsError = true
	}
}

type opts struct {
	interceptor        grpc.UnaryServerInterceptor
	ignoreStreamsError bool
}

func NewServer(optFns ...optFn) *Server {
	o := opts{}

	for _, fn := range optFns {
		fn(&o)
	}

	return &Server{
		services:    make(map[string]*service),
		interceptor: o.interceptor,
		opts:        o,
	}
}

func (s *Server) RegisterService(sd *grpc.ServiceDesc, ss interface{}) {
	if ss != nil {
		ht := reflect.TypeOf(sd.HandlerType).Elem()
		st := reflect.TypeOf(ss)
		if !st.Implements(ht) {
			log.Fatalf("gez: Server.RegisterService found the handler of type %v that does not satisfy %v", st, ht)
		}
	}

	if !s.opts.ignoreStreamsError && len(sd.Streams) != 0 {
		log.Fatalf("gez: Server.RegisterService found the service %s is trying to register unsupported streams", sd.ServiceName)
	}

	if _, ok := s.services[sd.ServiceName]; ok {
		log.Fatalf("gez: Server.RegisterService found the service %s is being registered twice", sd.ServiceName)
	}

	svc := &service{
		name:    sd.ServiceName,
		svc:     ss,
		methods: make(map[string]*method),
	}

	for _, m := range sd.Methods {
		svc.methods[m.MethodName] = &method{
			name:    m.MethodName,
			handler: methodHandler(m.Handler),
		}
	}

	s.services[sd.ServiceName] = svc
}

func (s *Server) Handle(w http.ResponseWriter, r *http.Request) {
	var req Request
	if err := jsoniter.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err)
		RespondWithStatus(
			w,
			status.New(codes.InvalidArgument, "invalid JSON"),
		)
		return
	}
	if req.Data == nil {
		RespondWithStatus(
			w,
			status.New(codes.InvalidArgument, "no request data sent"),
		)
		return
	}

	methodParts := strings.Split(req.Method, "/")
	if len(methodParts) != 3 {
		RespondWithStatus(
			w,
			status.Newf(codes.Unimplemented, "invalid RPC method %q", req.Method),
		)
		return
	}
	service := methodParts[1]
	method := methodParts[2]
	svc, ok := s.services[service]
	if !ok {
		RespondWithStatus(
			w,
			status.Newf(codes.Unimplemented, "unknown RPC service %q", req.Method),
		)
		return
	}

	m, ok := svc.methods[method]
	if !ok {
		RespondWithStatus(
			w,
			status.Newf(codes.Unimplemented, "unknown RPC method for service %q", req.Method),
		)
		return
	}
	ctx := metadata.NewIncomingContext(r.Context(), metadata.MD(r.Header))
	ctx = metadata.NewOutgoingContext(ctx, metadata.New(nil))

	res, err := m.handler(
		svc.svc,
		ctx,
		func(in interface{}) error {
			if err := protojson.Unmarshal(req.Data, in.(proto.Message)); err != nil {
				return status.Error(codes.InvalidArgument, "invalid request data")
			}
			return nil
		},
		s.interceptor,
	)

	outgoingMd, ok := metadata.FromOutgoingContext(ctx)
	if ok {
		for k := range outgoingMd {
			w.Header().Add(k, strings.Join(outgoingMd.Get(k), ","))
		}
	}

	response := &Response{}
	if err == nil {
		data, err := protojson.Marshal(res.(proto.Message))
		if err != nil {
			RespondWithStatus(
				w,
				status.New(codes.Internal, "failed to marshal JSON response"),
			)
			return
		}
		response.Data = data
	} else {
		RespondWithStatus(w, status.Convert(err))
		return
	}
	Respond(
		w,
		response,
	)
}

func Respond(w http.ResponseWriter, r *Response) {
	w.Header().Add("Content-Type", "application/json")
	jsoniter.NewEncoder(w).Encode(r)
}

func RespondWithStatus(w http.ResponseWriter, s *status.Status) {
	out, err := protojson.Marshal(s.Proto())
	if err != nil {
		log.Fatal(err)
	}
	Respond(w, &Response{Status: out})
}
