package main

import (
	"context"
	"log"
	"net/http"
	"os"

	"gitlab.com/codyleyhan/gez"
	hellopb "gitlab.com/codyleyhan/gez/proto/hello"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

func main() {
	cc := gez.NewClient(http.DefaultClient, "http://localhost:3000/api/call")
	client := hellopb.NewHelloApiClient(cc)

	ctx := metadata.AppendToOutgoingContext(context.Background(), "X-Foo", "bar")
	var md metadata.MD
	res, err := client.Hello(ctx, &hellopb.HelloRequest{Msg: os.Args[1]}, grpc.Header(&md))
	if err != nil {
		log.Fatal(err)
	}
	log.Println(res)
	log.Println(md)
}
