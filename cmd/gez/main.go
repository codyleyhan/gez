package main

import (
	"context"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/codyleyhan/gez"
	hellopb "gitlab.com/codyleyhan/gez/proto/hello"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func main() {
	s := &Server{}
	i := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		log.Println("got request")
		resp, err := handler(ctx, req)
		log.Println("caller finished")

		return resp, err
	}

	gezServer := gez.NewServer(gez.WithInterceptor(i))
	hellopb.RegisterHelloApiServer(gezServer, s)

	r := chi.NewMux()

	r.Post("/api/call", gezServer.Handle)

	http.ListenAndServe(":3000", r)
}

var _ hellopb.HelloApiServer = (*Server)(nil)

type Server struct {
	hellopb.UnimplementedHelloApiServer
}

func (s *Server) Hello(ctx context.Context, req *hellopb.HelloRequest) (*hellopb.HelloResponse, error) {
	md, _ := metadata.FromIncomingContext(ctx)
	log.Println("got a request", md)
	outMd, _, _ := metadata.FromOutgoingContextRaw(ctx)
	outMd.Append("X-Foo", "bar")
	if req.GetMsg() == "error" {
		return nil, status.New(codes.AlreadyExists, "yeet").Err()
	}
	return &hellopb.HelloResponse{Msg: req.GetMsg()}, nil
}

func (s *Server) HealthCheck(ctx context.Context, req *hellopb.HealthCheckRequest) (*hellopb.HealthCheckResponse, error) {
	return &hellopb.HealthCheckResponse{Result: hellopb.HealthcheckResult_OK}, nil
}
