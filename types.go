package gez

import (
	"encoding/json"
)

type Request struct {
	Method string          `json:"method,omitempty"`
	Data   json.RawMessage `json:"data,omitempty"`
}

type Response struct {
	Data   json.RawMessage `json:"data,omitempty"`
	Status json.RawMessage `json:"status,omitempty"`
}
