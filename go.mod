module gitlab.com/codyleyhan/gez

go 1.17

require (
	github.com/go-chi/chi/v5 v5.0.7
	google.golang.org/genproto v0.0.0-20220204002441-d6cc3cc0770e
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20220204135822-1c1b9b1eba6a // indirect
	golang.org/x/text v0.3.7 // indirect
)

require (
	github.com/json-iterator/go v1.1.12
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
)
